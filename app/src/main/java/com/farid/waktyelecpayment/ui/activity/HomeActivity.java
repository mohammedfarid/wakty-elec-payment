package com.farid.waktyelecpayment.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.farid.waktyelecpayment.R;
import com.farid.waktyelecpayment.adapter.ProviderRvAdapter;
import com.farid.waktyelecpayment.apiHandler.BaseApi;
import com.farid.waktyelecpayment.apiHandler.BaseApiHandler;
import com.farid.waktyelecpayment.model.response.AuthResponse;
import com.farid.waktyelecpayment.model.response.Providers;
import com.farid.waktyelecpayment.model.response.ProvidersResponse;
import com.farid.waktyelecpayment.ui.dialog.CustomDialog;
import com.farid.waktyelecpayment.utils.LocaleHelper;
import com.farid.waktyelecpayment.utils.PrefsManager;
import com.farid.waktyelecpayment.utils.RecyclerItemClickListener;
import com.farid.waktyelecpayment.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farid.waktyelecpayment.App.PHP_PASSWORD;
import static com.farid.waktyelecpayment.App.PHP_USERNAME;
import static com.farid.waktyelecpayment.App.mRobotoMedium;

public class HomeActivity extends AppCompatActivity {

    public static final String GAUGE_COUNT = "gauge_count";

    private ProvidersResponse mProvidersResponse;
    private List<Providers> mProvidersList;

    public RelativeLayout mHomeRl;
    private TextView mEmptyDataTv;
    private RecyclerView mProvidersView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayoutManager mLinearLayoutManager;
    private ProviderRvAdapter mProviderRvAdapter;

    private LinearLayout mLoadingLayout;
    private ProgressBar mLoadingProgressBar;

    public BaseApi mServiceApi;

    private PrefsManager mPrefsManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        mPrefsManager = new PrefsManager(newBase);
        super.attachBaseContext(LocaleHelper.wrap(newBase, mPrefsManager.getAppLanguage()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mHomeRl = findViewById(R.id.home_layout);
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mProvidersView = findViewById(R.id.provider_list);
        mEmptyDataTv = findViewById(R.id.empty_provider);
        mEmptyDataTv.setTypeface(mRobotoMedium);

        mLoadingLayout = findViewById(R.id.layout_loading);
        mLoadingProgressBar = findViewById(R.id.progress_bar_loading);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.comp));
            if (mPrefsManager.getAppLanguage().equals("ar")) {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            } else {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                openHomeActivity(mPrefsManager.loadAuthenticationData());
            }
        });
        mServiceApi = BaseApiHandler.setupBaseApi().create(BaseApi.class);

        openHomeActivity(mPrefsManager.loadAuthenticationData());

        mProvidersView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FragmentManager fm = getSupportFragmentManager();
                CustomDialog customDialog = CustomDialog.newInstance("Some Title");
                customDialog.show(fm, "fragment_edit_name");
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.trans_menu:
                if (mPrefsManager.getAppLanguage().equals("ar")) {
                    mPrefsManager.setAppLanguage("en");
                } else {
                    mPrefsManager.setAppLanguage("ar");
                }
                recreate();
                return true;
            case R.id.logout_menu:
                mPrefsManager.setLoggedIn(false);
                Intent mIntent = new Intent(HomeActivity.this,LoginActivity.class);
                startActivity(mIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openHomeActivity(AuthResponse authResponse) {
        Utils.showLoading(mLoadingLayout, mLoadingProgressBar);
        mHomeRl.setEnabled(false);
        mSwipeRefreshLayout.setRefreshing(true);
        Call<ProvidersResponse> providersResponseCall = mServiceApi.getElectricityProvider(PHP_USERNAME, PHP_PASSWORD, authResponse.getUserId());
        providersResponseCall.enqueue(new Callback<ProvidersResponse>() {
            @Override
            public void onResponse(Call<ProvidersResponse> call, Response<ProvidersResponse> response) {
                Utils.hideLoading(mLoadingLayout, mLoadingProgressBar);
                if (response.isSuccessful()) {
                    try {
                        ProvidersResponse mProvidersResponse = response.body();

                        if (mProvidersResponse != null) {
                            initRecycledView(mProvidersResponse);
                        } else {
                            mHomeRl.setEnabled(true);
                            Utils.showSnackBar(getResources().getString(R.string.no_data), mHomeRl);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    } catch (Exception e) {
                        mHomeRl.setEnabled(true);
                        Utils.showSnackBar(e.getMessage(), mHomeRl);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                } else {
                    mHomeRl.setEnabled(true);
                    Utils.showSnackBar(response.message(), mHomeRl);
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ProvidersResponse> call, Throwable t) {
                Utils.hideLoading(mLoadingLayout, mLoadingProgressBar);
                Utils.showSnackBar(t.getMessage(), mHomeRl);
                mHomeRl.setEnabled(true);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void initRecycledView(ProvidersResponse providersResponse) {
        mProvidersResponse = providersResponse;
        if (mProvidersResponse != null) {
            if (mProvidersResponse.getStatus() == 1) {
                mProvidersList = mProvidersResponse.getProviders();
                mEmptyDataTv.setVisibility(View.GONE);
                mProvidersView.setVisibility(View.VISIBLE);
                mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                mProvidersView.setLayoutManager(mLinearLayoutManager);
                mProviderRvAdapter = new ProviderRvAdapter(mProvidersList, this);
                mProvidersView.setAdapter(mProviderRvAdapter);
                mSwipeRefreshLayout.setRefreshing(false);
            } else {
                mEmptyDataTv.setVisibility(View.VISIBLE);
                mProvidersView.setVisibility(View.GONE);
            }
        } else {
            mEmptyDataTv.setVisibility(View.VISIBLE);
            mProvidersView.setVisibility(View.GONE);
        }
    }

    public void callPayment(String gaugeCount) {
        Intent mIntent = new Intent(HomeActivity.this, CallActivity.class);

        mIntent.putExtra(GAUGE_COUNT, gaugeCount);

        startActivity(mIntent);
    }

}
