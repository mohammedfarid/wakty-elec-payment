package com.farid.waktyelecpayment.ui.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.farid.waktyelecpayment.R;
import com.farid.waktyelecpayment.ui.activity.CallActivity;
import com.farid.waktyelecpayment.ui.activity.HomeActivity;
import com.farid.waktyelecpayment.utils.Utils;

import static com.farid.waktyelecpayment.App.mRobotoMedium;

public class CustomDialogPin extends DialogFragment implements
        View.OnClickListener {

    public CallActivity mCallActivity;
    public AlertDialog mAlertDialog;
    public Button yes, no;
    public TextView mTitle;
    public EditText mDataEnter;


    public CustomDialogPin() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallActivity = (CallActivity)getActivity();
    }

    public static CustomDialogPin newInstance(String title) {
        CustomDialogPin frag = new CustomDialogPin();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_pin_code, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTitle = view.findViewById(R.id.txt_dia);
        mTitle.setTypeface(mRobotoMedium);
        mDataEnter = view.findViewById(R.id.gauge_counter);
        mDataEnter.setTypeface(mRobotoMedium);
        mDataEnter.requestFocus();
        yes = view.findViewById(R.id.btn_yes);
        yes.setTypeface(mRobotoMedium);
        no = view.findViewById(R.id.btn_no);
        no.setTypeface(mRobotoMedium);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
        mDataEnter.requestFocus();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                String pinCode = mDataEnter.getText().toString().trim();
                if (!pinCode.isEmpty() && !pinCode.equals("")) {
                    mCallActivity.BillSuccessful();
                } else {
                    Utils.showSnackBar(mCallActivity.getResources().getString(R.string.msg_pin_code), mCallActivity.mCallRl);
                    dismiss();
                }
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

}
