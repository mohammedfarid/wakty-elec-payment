package com.farid.waktyelecpayment.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.farid.waktyelecpayment.R;
import com.farid.waktyelecpayment.adapter.BillsRvAdapter;
import com.farid.waktyelecpayment.apiHandler.BaseApi;
import com.farid.waktyelecpayment.apiHandler.BaseApiHandler;
import com.farid.waktyelecpayment.model.response.BillsCheckResponse;
import com.farid.waktyelecpayment.utils.LocaleHelper;
import com.farid.waktyelecpayment.utils.PrefsManager;
import com.farid.waktyelecpayment.utils.Utils;

import java.util.List;

import static com.farid.waktyelecpayment.App.mRobotoMedium;
import static com.farid.waktyelecpayment.ui.activity.HomeActivity.GAUGE_COUNT;

public class CallActivity extends AppCompatActivity {

    public RelativeLayout mCallRl;
    private TextView mEmptyDataTv;
    private RecyclerView mBillsView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayoutManager mLinearLayoutManager;
    private BillsRvAdapter mBillsRvAdapter;

    private LinearLayout mLoadingLayout;
    private ProgressBar mLoadingProgressBar;

    private List<BillsCheckResponse> mBillsCheckResponseList;

    public BaseApi mServiceApi;

    private PrefsManager mPrefsManager;

    private String gaugeCount;

    @Override
    protected void attachBaseContext(Context newBase) {
        mPrefsManager = new PrefsManager(newBase);
        super.attachBaseContext(LocaleHelper.wrap(newBase, mPrefsManager.getAppLanguage()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        gaugeCount = getIntent().getStringExtra(GAUGE_COUNT);

        mCallRl = findViewById(R.id.call_layout);
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mBillsView = findViewById(R.id.bill_list);
        mEmptyDataTv = findViewById(R.id.empty_bill);
        mEmptyDataTv.setTypeface(mRobotoMedium);

        mLoadingLayout = findViewById(R.id.layout_loading);
        mLoadingProgressBar = findViewById(R.id.progress_bar_loading);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.bill));
            if (mPrefsManager.getAppLanguage().equals("ar")) {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            } else {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callBills(gaugeCount);
            }
        });
        mServiceApi = BaseApiHandler.setupBaseApi().create(BaseApi.class);

        callBills(gaugeCount);

//        mBillsView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
////                CustomDialogPin customDialogPin = new CustomDialogPin(CallActivity.this);
////                customDialogPin.show();
//            }
//        }));
    }

    private void callBills(String gaugeCount) {
        Utils.showLoading(mLoadingLayout, mLoadingProgressBar);
        mCallRl.setEnabled(false);
        mSwipeRefreshLayout.setRefreshing(true);
        initRecycledView();
//        Call<ProvidersResponse> providersResponseCall = mServiceApi.getElectricityProvider(PHP_USERNAME, PHP_PASSWORD, authResponse.getUserId());
//        providersResponseCall.enqueue(new Callback<ProvidersResponse>() {
//            @Override
//            public void onResponse(Call<ProvidersResponse> call, Response<ProvidersResponse> response) {
//                Utils.hideLoading(mLoadingLayout, mLoadingProgressBar);
//                if (response.isSuccessful()) {
//                    try {
//                        ProvidersResponse mProvidersResponse = response.body();
//
//                        if (mProvidersResponse != null) {
//                            initRecycledView(mProvidersResponse);
//                        } else {
//                            mCallRl.setEnabled(true);
//                            Utils.showSnackBar(getResources().getString(R.string.no_data), mCallRl);
//                            mSwipeRefreshLayout.setRefreshing(false);
//                        }
//                    } catch (Exception e) {
//                        mCallRl.setEnabled(true);
//                        Utils.showSnackBar(e.getMessage(), mCallRl);
//                        mSwipeRefreshLayout.setRefreshing(false);
//                    }
//                } else {
//                    mCallRl.setEnabled(true);
//                    Utils.showSnackBar(response.message(), mCallRl);
//                    mSwipeRefreshLayout.setRefreshing(false);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ProvidersResponse> call, Throwable t) {
//                Utils.hideLoading(mLoadingLayout, mLoadingProgressBar);
//                Utils.showSnackBar(t.getMessage(), mCallRl);
//                mCallRl.setEnabled(true);
//                mSwipeRefreshLayout.setRefreshing(false);
//            }
//        });
        Utils.hideLoading(mLoadingLayout, mLoadingProgressBar);
        mCallRl.setEnabled(true);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void initRecycledView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mBillsView.setLayoutManager(mLinearLayoutManager);
        mBillsRvAdapter = new BillsRvAdapter(mBillsCheckResponseList, this);
        mBillsView.setAdapter(mBillsRvAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.trans_menu:
                if (mPrefsManager.getAppLanguage().equals("ar")) {
                    mPrefsManager.setAppLanguage("en");
                } else {
                    mPrefsManager.setAppLanguage("ar");
                }
                recreate();
                return true;
            case R.id.logout_menu:
                mPrefsManager.setLoggedIn(false);
                Intent mIntent = new Intent(CallActivity.this, LoginActivity.class);
                startActivity(mIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void BillSuccessful() {
        Utils.showSnackBar(getResources().getString(R.string.bill_succ), mCallRl);
    }
}
