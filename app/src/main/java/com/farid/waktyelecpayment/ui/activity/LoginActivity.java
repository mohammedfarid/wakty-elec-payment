package com.farid.waktyelecpayment.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.farid.waktyelecpayment.App;
import com.farid.waktyelecpayment.R;
import com.farid.waktyelecpayment.apiHandler.BaseApi;
import com.farid.waktyelecpayment.apiHandler.BaseApiHandler;
import com.farid.waktyelecpayment.model.response.AuthResponse;
import com.farid.waktyelecpayment.utils.LocaleHelper;
import com.farid.waktyelecpayment.utils.PrefsManager;
import com.farid.waktyelecpayment.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.farid.waktyelecpayment.App.PHP_PASSWORD;
import static com.farid.waktyelecpayment.App.PHP_USERNAME;

public class LoginActivity extends AppCompatActivity {

    private RelativeLayout mLoginRl;
    private EditText mUserEt;
    private EditText mPassEt;

    private ImageView mUserIv;
    private ImageView mPassIv;
    private ImageView mTrans;

    private Button mLoginBt;

    private LinearLayout mLoadingLayout;
    private ProgressBar mLoadingProgressBar;

    private boolean showPassword = false;

    private PrefsManager mPrefsManager;
    public BaseApi mServiceApi;

    @Override
    protected void attachBaseContext(Context newBase) {
        mPrefsManager = new PrefsManager(newBase);
        super.attachBaseContext(LocaleHelper.wrap(newBase, mPrefsManager.getAppLanguage()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (mPrefsManager.getAppLanguage().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        mServiceApi = BaseApiHandler.setupBaseApi().create(BaseApi.class);

        mLoginRl = findViewById(R.id.login_rl);

        mUserEt = findViewById(R.id.username_ed);
        mUserEt.setTypeface(App.mRobotoMedium);
        mPassEt = findViewById(R.id.password_ed);
        mPassEt.setTypeface(App.mRobotoMedium);

        mUserIv = findViewById(R.id.username_iv);
        mPassIv = findViewById(R.id.password_iv);
        mTrans = findViewById(R.id.trans_iv);

        mLoginBt = findViewById(R.id.login_bt);
        mLoginBt.setTypeface(App.mRobotoMedium);

        mLoadingLayout = findViewById(R.id.layout_loading);
        mLoadingProgressBar = findViewById(R.id.progress_bar_loading);

        if (mPrefsManager.isLoggedIn()) {
            Intent mIntent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(mIntent);
            finish();
        }

        mPassIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!showPassword) {
                    mPassEt.setTransformationMethod(new HideReturnsTransformationMethod());
                    mPassIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_open_24dp));
                    showPassword = true;
                } else {
                    mPassEt.setTransformationMethod(new PasswordTransformationMethod());
                    mPassIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_24dp));
                    showPassword = false;
                }
            }
        });

        mLoginBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(mUserEt.getText().toString())) {
                    mUserEt.setError(getResources().getString(R.string.error_username));
                    return;
                }
                if (TextUtils.isEmpty(mPassEt.getText().toString())) {
                    mPassEt.setError(getResources().getString(R.string.error_password));
                    return;
                }
                callLogin();
            }
        });

        mTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPrefsManager.getAppLanguage().equals("ar")) {
                    mPrefsManager.setAppLanguage("en");
                } else {
                    mPrefsManager.setAppLanguage("ar");
                }
                recreate();
            }
        });
        Utils.setupHideKeyboard(LoginActivity.this, mLoginRl);

    }

    private void callLogin() {
        Utils.showLoading(mLoadingLayout, mLoadingProgressBar);
        mLoginRl.setEnabled(false);
        Call<AuthResponse> authResponseCall = mServiceApi.getLoginInfo(PHP_USERNAME, PHP_PASSWORD, mUserEt.getText().toString(), mPassEt.getText().toString());

        authResponseCall.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                Utils.hideLoading(mLoadingLayout, mLoadingProgressBar);
                if (response.isSuccessful()) {
                    try {
                        AuthResponse authResponse = response.body();
                        if (authResponse != null) {
                            if (authResponse.getPinCode() != null) {
                                mPrefsManager.setLoggedIn(true);
                                mPrefsManager.saveAuthenticationData(authResponse);
                                Intent mIntent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(mIntent);
                                finish();
                            } else {
                                mLoginRl.setEnabled(true);
                                Utils.showSnackBar(getResources().getString(R.string.no_data), mLoginRl);
                            }
                        } else {
                            mLoginRl.setEnabled(true);
                            Utils.showSnackBar(getResources().getString(R.string.no_data), mLoginRl);
                        }
                    } catch (Exception e) {
                        mLoginRl.setEnabled(true);
                        Utils.showSnackBar(e.getMessage(), mLoginRl);
                    }
                } else {
                    mLoginRl.setEnabled(true);
                    Utils.showSnackBar(response.message(), mLoginRl);
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Utils.hideLoading(mLoadingLayout, mLoadingProgressBar);
                Utils.showSnackBar(t.getMessage(), mLoginRl);
                mLoginRl.setEnabled(true);
            }
        });
    }


}
