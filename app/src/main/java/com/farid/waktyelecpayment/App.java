package com.farid.waktyelecpayment;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;

import com.farid.waktyelecpayment.utils.LocaleHelper;
import com.farid.waktyelecpayment.utils.PrefsManager;

public class App extends Application {
    public static final String API_BASE_URL = "https://www.wakty.com/webservice/";
    public static final String PHP_USERNAME = "562318";
    public static final String PHP_PASSWORD = "23465";

    private PrefsManager mPrefsManger;
    public static Typeface mRobotoMedium;

    public static App instance;

    @Override
    protected void attachBaseContext(Context base) {
        mPrefsManger = new PrefsManager(base);
        super.attachBaseContext(LocaleHelper.wrap(base, mPrefsManger.getAppLanguage()));
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mRobotoMedium = Typeface.createFromAsset(this.getApplicationContext().getAssets(), "fonts/Roboto-Medium.ttf");
    }

    public static App getInstance() {
        return instance;
    }

    public static boolean hasNetwork() {
        return instance.checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (cm != null) {
            networkInfo = cm.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.isConnected();
    }

}
