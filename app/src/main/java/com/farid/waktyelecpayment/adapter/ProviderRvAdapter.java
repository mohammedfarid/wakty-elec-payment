package com.farid.waktyelecpayment.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farid.waktyelecpayment.R;
import com.farid.waktyelecpayment.model.response.Providers;

import java.util.List;

import static com.farid.waktyelecpayment.App.mRobotoMedium;

public class ProviderRvAdapter extends RecyclerView.Adapter<ProviderRvAdapter.ViewHolder> {

    private Context mContext;
    private List<Providers> mProvidersList;

    public ProviderRvAdapter(List<Providers> providersList, Context context) {
        this.mProvidersList = providersList;
        this.mContext = context;
    }

    public Providers getValueAt(int position) {
        return mProvidersList.get(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_providers_list, viewGroup, false);
        return new ProviderRvAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Providers mProviders = getValueAt(i);
        if (mProviders != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (mContext.getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL)
                    viewHolder.mNameProviderTv.setText(mProviders.getName());
                else
                    viewHolder.mNameProviderTv.setText(mProviders.getMacName());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mProvidersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final TextView mNameProviderTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            mNameProviderTv = itemView.findViewById(R.id.name_provider);
            mNameProviderTv.setTypeface(mRobotoMedium);
        }
    }
}
