package com.farid.waktyelecpayment.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.farid.waktyelecpayment.R;
import com.farid.waktyelecpayment.model.response.BillsCheckResponse;
import com.farid.waktyelecpayment.ui.activity.CallActivity;
import com.farid.waktyelecpayment.ui.dialog.CustomDialog;
import com.farid.waktyelecpayment.ui.dialog.CustomDialogPin;

import java.util.List;

import static com.farid.waktyelecpayment.App.mRobotoMedium;

public class BillsRvAdapter extends RecyclerView.Adapter<BillsRvAdapter.ViewHolder> {

    private Context mContext;
    private List<BillsCheckResponse> mBillsCheckResponseList;
    private CallActivity mCallActivity;

    public BillsRvAdapter(List<BillsCheckResponse> billsCheckResponseList, CallActivity callActivity) {
        this.mBillsCheckResponseList = billsCheckResponseList;
        this.mCallActivity = callActivity;
    }

    public Object getValueAt(int position) {
        return "";
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_bill_list, viewGroup, false);
        return new BillsRvAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
//        Providers mProviders = getValueAt(i);
//        if (mProviders != null) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                if (mContext.getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL)
//                    viewHolder.mNameProviderTv.setText(mProviders.getName());
//                else
//                    viewHolder.mNameProviderTv.setText(mProviders.getMacName());
//            }
//        }
        viewHolder.mBillPayBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = mCallActivity.getSupportFragmentManager();
                CustomDialogPin customDialogPin = CustomDialogPin.newInstance("");
                customDialogPin.show(fm, "fragment_edit_name");
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final TextView mBillNumberTitleTv;
        public final TextView mBillNumberTv;
        public final TextView mBillAmountTitleTv;
        public final TextView mBillAmountTv;

        public final Button mBillPayBt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            mBillNumberTitleTv = itemView.findViewById(R.id.bill_number_title);
            mBillNumberTitleTv.setTypeface(mRobotoMedium);

            mBillNumberTv = itemView.findViewById(R.id.bill_number);
            mBillNumberTv.setTypeface(mRobotoMedium);

            mBillAmountTitleTv = itemView.findViewById(R.id.bill_amount_title);
            mBillAmountTitleTv.setTypeface(mRobotoMedium);

            mBillAmountTv = itemView.findViewById(R.id.bill_amount);
            mBillAmountTv.setTypeface(mRobotoMedium);

            mBillPayBt = itemView.findViewById(R.id.bill_pay);
            mBillPayBt.setTypeface(mRobotoMedium);
        }
    }
}
