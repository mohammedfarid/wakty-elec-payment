package com.farid.waktyelecpayment.apiHandler;


import com.farid.waktyelecpayment.model.response.AuthResponse;
import com.farid.waktyelecpayment.model.response.BillsCheckResponse;
import com.farid.waktyelecpayment.model.response.BillsPaymentResponses;
import com.farid.waktyelecpayment.model.response.ProvidersResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BaseApi {

    @GET("users/login/{phpUserName}/{phpPassword}/{username}/{password}")
    Call<AuthResponse> getLoginInfo(@Path("phpUserName") String PHPUsername,
                                    @Path("phpPassword") String PHPPassword,
                                    @Path("username") String Username,
                                    @Path("password") String Password);

    @GET("electricity/get_electricity_providers/{phpUserName}/{phpPassword}/{userId}")
    Call<ProvidersResponse> getElectricityProvider(@Path("phpUserName") String UserName,
                                                   @Path("phpPassword") String Password,
                                                   @Path("userId") String UserId);

    @GET("electricity/bill_inquiry/{phpUserName}/{phpPassword}/{userId}/{counterId}/{providerId}")
    Call<BillsCheckResponse> getBillCheck(@Path("phpUserName") String UserName,
                                          @Path("phpPassword") String Password,
                                          @Path("userId") String UserId,
                                          @Path("counterId") String CounterId,
                                          @Path("providerId") String ProviderId);

    @POST("electricity/bill_payment/{phpUserName}/{phpPassword}")
    Call<BillsPaymentResponses> getBillPayment(@Path("phpUserName") String UserName,
                                               @Path("phpPassword") String Password,
                                               @Query("userId") String UserId,
                                               @Query("counterId") String CounterId,
                                               @Query("providerId") String ProviderId,
                                               @Query("amount") String Amount,
                                               @Query("ePayRecord") String EPayRecord);

}
