package com.farid.waktyelecpayment.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.farid.waktyelecpayment.model.response.AuthResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;


public class PrefsManager {

    private static final String FILE_NAME = "waktyElecPayment Preferences";

    private Context mContext;
    private SharedPreferences mSharedPreferences;

    //Keys
    private static final String KEY_LOGGED = "LoginOurNot";
    private static final String KEY_LANGUAGE = "ApplicationLanguage";
    private static final String KEY_AUTHENTICATION = "AuthenticationData";

    //Default Values
    private static final boolean DEFAULT_LOGGED = false;
    private static final String DEFAULT_LANGUAGE = "en";
    private static final String DEFAULT_AUTHENTICATION = null;


    public PrefsManager(Context context) {
        this.mContext = context;
        mSharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public void setLoggedIn(boolean loggedIn) {
        mSharedPreferences.edit().putBoolean(KEY_LOGGED, loggedIn).apply();
    }

    public boolean isLoggedIn() {
        return mSharedPreferences.getBoolean(KEY_LOGGED, DEFAULT_LOGGED);
    }

    public void setAppLanguage(String language) {
        mSharedPreferences.edit().putString(KEY_LANGUAGE, language).apply();
    }

    public String getAppLanguage() {
        return mSharedPreferences.getString(KEY_LANGUAGE, DEFAULT_LANGUAGE);
    }

    public void saveAuthenticationData(AuthResponse authData) {
        String json = new Gson().toJson(authData);
        mSharedPreferences.edit().putString(KEY_AUTHENTICATION, json).apply();
    }

    public AuthResponse loadAuthenticationData() {
        String json = mSharedPreferences.getString(KEY_AUTHENTICATION, DEFAULT_AUTHENTICATION);
        Type type = new TypeToken<AuthResponse>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

}
