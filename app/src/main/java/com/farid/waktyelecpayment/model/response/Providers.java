package com.farid.waktyelecpayment.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Providers implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("MacName")
    @Expose
    private String macName;
    @SerializedName("Req-Code")
    @Expose
    private String reqCode;
    @SerializedName("admin_id")
    @Expose
    private String adminId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("deleted")
    @Expose
    private String deleted;

    protected Providers(Parcel in) {
        id = in.readString();
        name = in.readString();
        macName = in.readString();
        reqCode = in.readString();
        adminId = in.readString();
        status = in.readString();
        date = in.readString();
        deleted = in.readString();
    }

    public static final Creator<Providers> CREATOR = new Creator<Providers>() {
        @Override
        public Providers createFromParcel(Parcel in) {
            return new Providers(in);
        }

        @Override
        public Providers[] newArray(int size) {
            return new Providers[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacName() {
        return macName;
    }

    public void setMacName(String macName) {
        this.macName = macName;
    }

    public String getReqCode() {
        return reqCode;
    }

    public void setReqCode(String reqCode) {
        this.reqCode = reqCode;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(macName);
        parcel.writeString(reqCode);
        parcel.writeString(adminId);
        parcel.writeString(status);
        parcel.writeString(date);
        parcel.writeString(deleted);
    }
}
