package com.farid.waktyelecpayment.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProvidersResponse implements Parcelable {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("providers")
    @Expose
    private List<Providers> providers = null;
    @SerializedName("data")
    @Expose
    private String Data;

    protected ProvidersResponse(Parcel in) {
        if (in.readByte() == 0) {
            status = null;
        } else {
            status = in.readInt();
        }
        providers = in.createTypedArrayList(Providers.CREATOR);
        Data = in.readString();
    }

    public static final Creator<ProvidersResponse> CREATOR = new Creator<ProvidersResponse>() {
        @Override
        public ProvidersResponse createFromParcel(Parcel in) {
            return new ProvidersResponse(in);
        }

        @Override
        public ProvidersResponse[] newArray(int size) {
            return new ProvidersResponse[size];
        }
    };

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Providers> getProviders() {
        return providers;
    }

    public void setProviders(List<Providers> providers) {
        this.providers = providers;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (status == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(status);
        }
        parcel.writeTypedList(providers);
        parcel.writeString(Data);
    }
}
