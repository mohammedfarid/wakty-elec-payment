package com.farid.waktyelecpayment.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillsCheckResponse {

    @SerializedName("RESPONSE")
    @Expose
    private RESPONSE rESPONSE;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("provider_id")
    @Expose
    private String providerId;
    @SerializedName("ePayBillRecordID")
    @Expose
    private Integer ePayBillRecordID;
    @SerializedName("billNumber")
    @Expose
    private Integer billNumber;
    @SerializedName("billValue")
    @Expose
    private Integer billValue;
    @SerializedName("efinance_fees")
    @Expose
    private Integer efinanceFees;
    @SerializedName("customer_code")
    @Expose
    private Integer customerCode;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("bills_count")
    @Expose
    private Integer billsCount;
    @SerializedName("default_amount")
    @Expose
    private Integer defaultAmount;
    @SerializedName("bill_number_val")
    @Expose
    private Integer billNumberVal;
    @SerializedName("bill_date")
    @Expose
    private String billDate;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("service_fees")
    @Expose
    private Integer serviceFees;
    @SerializedName("data")
    @Expose
    private String Data;

    public RESPONSE getRESPONSE() {
        return rESPONSE;
    }

    public void setRESPONSE(RESPONSE rESPONSE) {
        this.rESPONSE = rESPONSE;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public Integer getEPayBillRecordID() {
        return ePayBillRecordID;
    }

    public void setEPayBillRecordID(Integer ePayBillRecordID) {
        this.ePayBillRecordID = ePayBillRecordID;
    }

    public Integer getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(Integer billNumber) {
        this.billNumber = billNumber;
    }

    public Integer getBillValue() {
        return billValue;
    }

    public void setBillValue(Integer billValue) {
        this.billValue = billValue;
    }

    public Integer getEfinanceFees() {
        return efinanceFees;
    }

    public void setEfinanceFees(Integer efinanceFees) {
        this.efinanceFees = efinanceFees;
    }

    public Integer getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(Integer customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getBillsCount() {
        return billsCount;
    }

    public void setBillsCount(Integer billsCount) {
        this.billsCount = billsCount;
    }

    public Integer getDefaultAmount() {
        return defaultAmount;
    }

    public void setDefaultAmount(Integer defaultAmount) {
        this.defaultAmount = defaultAmount;
    }

    public Integer getBillNumberVal() {
        return billNumberVal;
    }

    public void setBillNumberVal(Integer billNumberVal) {
        this.billNumberVal = billNumberVal;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getServiceFees() {
        return serviceFees;
    }

    public void setServiceFees(Integer serviceFees) {
        this.serviceFees = serviceFees;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }
}
