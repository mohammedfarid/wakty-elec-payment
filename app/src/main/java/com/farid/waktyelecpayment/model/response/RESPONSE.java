package com.farid.waktyelecpayment.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RESPONSE {
    @SerializedName("TERMINALID")
    @Expose
    private String tERMINALID;
    @SerializedName("LOCALDATETIME")
    @Expose
    private String lOCALDATETIME;
    @SerializedName("SERVERDATETIME")
    @Expose
    private String sERVERDATETIME;
    @SerializedName("TXID")
    @Expose
    private String tXID;
    @SerializedName("HOSTTXID")
    @Expose
    private String hOSTTXID;
    @SerializedName("LIMIT")
    @Expose
    private String lIMIT;
    @SerializedName("RESULT")
    @Expose
    private String rESULT;
    @SerializedName("RESULTTEXT")
    @Expose
    private String rESULTTEXT;

    public String getTERMINALID() {
        return tERMINALID;
    }

    public void setTERMINALID(String tERMINALID) {
        this.tERMINALID = tERMINALID;
    }

    public String getLOCALDATETIME() {
        return lOCALDATETIME;
    }

    public void setLOCALDATETIME(String lOCALDATETIME) {
        this.lOCALDATETIME = lOCALDATETIME;
    }

    public String getSERVERDATETIME() {
        return sERVERDATETIME;
    }

    public void setSERVERDATETIME(String sERVERDATETIME) {
        this.sERVERDATETIME = sERVERDATETIME;
    }

    public String getTXID() {
        return tXID;
    }

    public void setTXID(String tXID) {
        this.tXID = tXID;
    }

    public String getHOSTTXID() {
        return hOSTTXID;
    }

    public void setHOSTTXID(String hOSTTXID) {
        this.hOSTTXID = hOSTTXID;
    }

    public String getLIMIT() {
        return lIMIT;
    }

    public void setLIMIT(String lIMIT) {
        this.lIMIT = lIMIT;
    }

    public String getRESULT() {
        return rESULT;
    }

    public void setRESULT(String rESULT) {
        this.rESULT = rESULT;
    }

    public String getRESULTTEXT() {
        return rESULTTEXT;
    }

    public void setRESULTTEXT(String rESULTTEXT) {
        this.rESULTTEXT = rESULTTEXT;
    }
}
